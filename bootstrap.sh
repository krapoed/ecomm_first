#!/bin/bash
#"stdin: is not a tty" Error fix ---------------------------
#uncomment 5 strings below if errors are appeared
#export LANGUAGE=en_US.UTF-8
#export LANG=en_US.UTF-8
#export LC_ALL=en_US.UTF-8
#locale-gen en_US.UTF-8
#dpkg-reconfigure locales
#-----------------------------------------------------------
#update, install
echo -e "\n\n\t[INFO]Installation is running:" 
sudo apt-get update
sudo apt-get -y install nginx
sudo apt-get -y install postgresql postgresql-contrib
sudo apt-get -y install python-pip
sudo apt-get -y install libtiff5-dev libjpeg8-dev zlib1g-dev libfreetype6-dev liblcms2-dev libwebp-dev tcl8.6-dev tk8.6-dev python-tk python-dev
sudo apt-get -y install git
sudo pip install gunicorn
sudo apt-get -y install python-psycopg2
echo -e "\n\n\t[INFO]installation has been finished." 

# database
echo -e "\n\n\t[INFO]Postgres database initialization:" 
sudo su postgres -c "createdb auto"
sudo -u postgres psql -c "CREATE USER auto WITH PASSWORD '123456'"
sudo -u postgres psql -c "GRANT ALL PRIVILEGES ON DATABASE auto TO auto"
echo -e "\t[INFO]database=auto user=auto were created."

echo -e "\n\n\t[INFO]Server IP config:=============================================="
ifconfig | grep "inet addr:"
echo -e "\t====================================================================="
echo -e "\n\n\t******Use "
echo -e "\t\t ---> fab vagrant uname deploy <---"
echo -e "\t\t ---> fab vagrant uname start  <---"
echo -e "\t\t\t\t for deploy/start server******"
