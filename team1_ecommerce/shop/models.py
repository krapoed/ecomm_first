from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from django.core.urlresolvers import reverse

from utils import unique_slugify


class Category(models.Model):
    """
    Class category
    """

    class Meta:
        verbose_name = 'Category'
        verbose_name_plural = 'Categories'

    name = models.CharField(max_length=50, unique=True)
    slug = models.SlugField(unique=True, blank=True)

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if len(self.slug) == 0:
            unique_slugify(self, self.name)

        super(Category, self).save(force_insert=force_insert, force_update=force_update, using=using,
                                   update_fields=update_fields)

    def get_absolute_url(self):
        return reverse('shop_category', kwargs={'slug': self.slug})

    def __unicode__(self):
        return self.name


class Tag(models.Model):
    tag = models.CharField(max_length=20, unique=True)
    slug = models.SlugField(unique=True, blank=True)

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if len(self.slug) == 0:
            unique_slugify(self, self.tag)

        super(Tag, self).save(force_insert=force_insert, force_update=force_update, using=using,
                              update_fields=update_fields)

    def get_absolute_url(self):
        return reverse('shop_tag', kwargs={'slug': self.slug})

    def __unicode__(self):
        return self.tag


class Rating(models.Model):
    CHOICES_RATING = (
        ('0', 'No rated'),
        ('1', 'Poor'),
        ('2', 'Fair'),
        ('3', 'Average'),
        ('4', 'Good'),
        ('5', 'Excellent'),
    )

    rating = models.CharField(max_length=1, choices=CHOICES_RATING, default='0')
    average_rating = models.FloatField(default=0)
    sum_rating = models.IntegerField(default=0)
    rating_counter = models.IntegerField(default=0)
    name = models.CharField(max_length=50)

    def count_rating(self):
        reviews = self.goods.review_set.all()
        self.average_rating = 0
        self.rating_counter = 0
        self.sum_rating = 0
        for review in reviews:
            if review.rating:
                self.rating_counter += 1
            self.sum_rating += review.rating
        if self.sum_rating and self.rating_counter:
            self.average_rating = float(self.sum_rating) / self.rating_counter

    def __unicode__(self):
        return self.name


class Manufacturer(models.Model):
    name = models.CharField(max_length=50, unique=True)
    logo = models.ImageField(blank=True)
    description = models.CharField(max_length=50, blank=True)
    slug = models.SlugField(blank=True, unique=True)

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if len(self.slug) == 0:
            unique_slugify(self, self.name)

        super(Manufacturer, self).save(force_insert=force_insert, force_update=force_update, using=using,
                                       update_fields=update_fields)

    def get_absolute_url(self):
        return reverse('shop_manufacturer', kwargs={'slug': self.slug})

    def __unicode__(self):
        return self.name


class Goods(models.Model):
    class Meta:
        verbose_name_plural = 'Goods'

    category = models.ForeignKey(Category)
    manufacturer = models.ForeignKey(Manufacturer)
    rating = models.OneToOneField(Rating, null=True, blank=True)
    tags = models.ManyToManyField(Tag, blank=True)
    name = models.CharField(max_length=50)
    description = models.TextField()
    price = models.FloatField(default=0)
    old_price = models.FloatField(default=0)
    in_stock = models.BooleanField(default=True)
    discount = models.FloatField(default=0)
    slug = models.SlugField(unique=True, blank=True)
    receipt_date = models.DateField(auto_now=True)

    def delete(self, using=None):
        if self.rating:
            self.rating.delete()
        super(Goods, self).delete(using=using)

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if not self.rating:
            self.rating = Rating.objects.create(name=self.name)

        if len(self.slug) == 0:
            unique_slugify(self, self.name)

        if self.old_price > self.price > 0 and self.discount == 0:
            self.discount = round((self.old_price - self.price) / self.old_price * 100)
        elif self.old_price == 0 and self.price > 0 and 100 > self.discount > 0:
            self.old_price = self.price
            self.price -= round(self.price * self.discount / 100)

        super(Goods, self).save(force_insert=force_insert, force_update=force_update, using=using,
                                update_fields=update_fields)

    def get_absolute_url(self):
        return reverse('shop_detail', kwargs={'slug': self.slug})

    def __unicode__(self):
        return self.name


class Image(models.Model):
    goods = models.ForeignKey(Goods)
    image = models.ImageField(blank=True)
    description = models.CharField(max_length=50, blank=True)


class Review(models.Model):
    from users.models import UserProfile
    goods = models.ForeignKey(Goods)
    author = models.ForeignKey(UserProfile)
    review_text = models.TextField(max_length=10000, blank=True)
    published = models.DateTimeField(auto_now=True)
    rating = models.IntegerField(default=0, validators=[MinValueValidator(0), MaxValueValidator(5)])

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        super(Review, self).save(force_insert=force_insert, force_update=force_update, using=using,
                                 update_fields=update_fields)
        self.goods.rating.count_rating()
        self.goods.rating.save()

    def delete(self, using=None):
        super(Review, self).delete(using=using)
        self.goods.rating.count_rating()
        self.goods.rating.save()

    def __unicode__(self):
        return self.author.username + '-' + self.goods.name
