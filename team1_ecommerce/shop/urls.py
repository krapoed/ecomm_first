from django.conf.urls import url

import views

urlpatterns = [
    url(r'^list/$', views.GoodsListView.as_view(), name='shop_list'),
    url(r'^$', views.shop_index, name='shop_index'),
    url(r'^list/most_rated/$', views.GoodsListView.as_view(ordering='-rating__average_rating'),
        name='shop_list_most_rated'),
    url(r'^list/lastest/$', views.GoodsListView.as_view(ordering='-receipt_date'),
        name='shop_list_lastest'),
    url(r'^add_review/$', views.GoodsReviewAddUpdate.as_view(), name='shop_review'),
    url(r'^del_review/$', views.ReviewDeleteView.as_view(), name='shop_review_delete'),
    url(r'^detail/(?P<slug>[-\w]+)/$', views.GoodsDetailView.as_view(), name='shop_detail'),
    url(r'^tag/(?P<slug>[-\w]+)/$', views.TagsListView.as_view(), name='shop_tag'),
    url(r'^category/(?P<slug>[-\w]+)/$', views.CategoryListView.as_view(), name='shop_category'),
    url(r'^brand/(?P<slug>[-\w]+)/$', views.ManufacturerListView.as_view(), name='shop_manufacturer'),
]
