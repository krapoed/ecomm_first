from django import template
from django.shortcuts import get_object_or_404

from shop.models import Review

register = template.Library()


@register.inclusion_tag('shop/rating_show.html')
def show_rating(goods, user=None):
    if user:
        rating = get_object_or_404(Review, goods=goods, author=user).rating
    else:
        rating = goods.rating.average_rating
    return {'rating': rating}


@register.simple_tag
def url_replace(request, field, value):
    dict_ = request.GET.copy()

    dict_[field] = value

    return dict_.urlencode()
