from django.contrib import admin

from .models import Category, Goods, Rating, Tag, Image, Review, Manufacturer


class ImageInline(admin.TabularInline):
    model = Image
    extra = 2


class RatingInline(admin.ModelAdmin):
    model = Rating
    fieldsets = (
        (None, {
            'fields': ('name', ('rating', 'average_rating'))
        }),
        ('Statistic', {
            'classes': ('collapse',),
            'fields': ('sum_rating', 'rating_counter')
        }),
    )


class ReviewInline(admin.TabularInline):
    model = Review
    readonly_fields = ('published',)
    fields = (
        ('author', 'published', 'rating'), 'review_text',
    )
    extra = 0


class GoodsAdmin(admin.ModelAdmin):
    list_display = ['name', 'category', 'manufacturer', 'receipt_date', 'price', 'in_stock', 'discount']
    fields = (('name', 'slug', 'receipt_date'), 'description', ('manufacturer', 'category', 'tags'),
              ('price', 'old_price', 'in_stock', 'discount'))
    readonly_fields = ('receipt_date',)
    inlines = [
        ImageInline, ReviewInline
    ]


class RatingAdmin(admin.ModelAdmin):
    fieldsets = (
        (None, {
            'fields': ('name', ('rating', 'average_rating'))
        }),
        ('Statistic', {
            'classes': ('collapse',),
            'fields': ('sum_rating', 'rating_counter')
        }),
    )


admin.site.register(Goods, GoodsAdmin)
admin.site.register(Rating, RatingAdmin)
admin.site.register(Category)
admin.site.register(Tag)
admin.site.register(Image)
admin.site.register(Review)
admin.site.register(Manufacturer)
