from braces.views import LoginRequiredMixin, AjaxResponseMixin, JSONResponseMixin, SuperuserRequiredMixin
from django.contrib import messages
from django.db import transaction
from django.shortcuts import render, redirect, get_object_or_404
from django.views.generic import View
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView

from .models import Goods, Review, Tag, Manufacturer, Category

ORDERING = {
    'new': '-receipt_date',
    'old': 'receipt_date',
    'has_discount': '-discount',
    'price_asc': 'price',
    'price_desc': '-price',
    'rating': '-rating__average_rating'
}

LIST_VIEW = [
    'normal',
    'string'
]


class GoodsReviewAddUpdate(LoginRequiredMixin, View):
    @transaction.non_atomic_requests
    def post(self, request, *args, **kwargs):
        goods_id = request.POST.get('goods_id', None)
        review_rating = request.POST.get('rating', '0')
        review_text = request.POST.get('review_text', '')
        delete = request.POST.get('delete', None)
        user = request.user
        goods = get_object_or_404(Goods, pk=goods_id)
        if delete:
            try:
                Review.objects.get(goods=goods, author=user).delete()
                messages.success(request, 'Your review was deleted!')
            except Review.DoesNotExist:
                messages.error(request, 'Review not found!')
            return redirect(request.META['HTTP_REFERER'])

        with transaction.atomic():
            review, created = Review.objects.get_or_create(goods=goods, author=user)
            review.rating = review_rating
            review.review_text = review_text
            review.save()
            if created:
                messages.success(request, 'Your review was submitted! Thank you!')
            else:
                messages.success(request, 'Your review was updated! Thank you!')
            return redirect(request.META['HTTP_REFERER'])


class GoodsListView(ListView):
    paginate_by = 6
    model = Goods
    ordering = '-in_stock'

    def post(self, request, **kwargs):
        if request.GET.get('set_sort_order') in ORDERING:
            request.session['sort_order'] = request.GET.get('set_sort_order')
        if request.POST.get('set_list_view') in LIST_VIEW:
            if request.POST.get('set_list_view') == 'normal':
                request.session['per_page'] = '6'
            else:
                request.session['per_page'] = '12'
            request.session['list_view'] = request.POST.get('set_list_view')

        if request.GET.get('set_per_page'):
            if request.GET.get('set_per_page').isdigit():
                if int(request.GET.get('set_per_page')) <= 100:
                    request.session['per_page'] = request.GET.get('set_per_page')

        return redirect(request.META['HTTP_REFERER'])

    def get_ordering(self):
        return ['-in_stock', ORDERING[self.request.session.get('sort_order')]]

    def get_paginate_by(self, queryset):
        return self.request.session.get('per_page', self.paginate_by)

    def get_context_data(self, **kwargs):
        context = super(GoodsListView, self).get_context_data()
        category = self.request.GET.getlist('category', None)
        tag = self.request.GET.getlist('tag', None)
        brand = self.request.GET.getlist('brand', None)
        if category:
            context['current_category'] = Category.objects.filter(slug__in=category)
        if tag:
            context['current_tag'] = Tag.objects.filter(slug__in=tag)
        if brand:
            context['current_brand'] = Manufacturer.objects.filter(slug__in=brand)
        return context

    def get_queryset(self):
        query = super(GoodsListView, self).get_queryset()
        sequence = {}
        category = self.request.GET.getlist('category', None)
        tags = self.request.GET.getlist('tag', None)
        brand = self.request.GET.getlist('brand', None)
        if category:
            sequence['category__slug__in'] = category
        if brand:
            sequence['manufacturer__slug__in'] = brand
        if sequence:
            query = query.filter(**sequence).distinct()
        for tag in tags:
            query = query.filter(tags__slug=tag)
        return query


class GoodsDetailView(DetailView):
    model = Goods

    def get_context_data(self, **kwargs):
        user = self.request.user
        context = super(GoodsDetailView, self).get_context_data(**kwargs)
        context['is_goods_rated_by_user'] = False
        if user.is_authenticated():
            goods = context['goods']
            try:
                review = goods.review_set.get(author=user)
                context['review_text'] = review.review_text
                context['rating'] = review.rating
                context['is_goods_rated_by_user'] = True
            except Review.DoesNotExist:
                context['review_text'] = ''
                context['rating'] = 0
        return context


def shop_index(request):
    latest = Goods.objects.all().order_by('-receipt_date')[:5]
    most_popular = Goods.objects.all().order_by('-rating__average_rating')[:5]
    return render(request, 'shop/index.html', {'latest': latest, 'most_popular': most_popular})


class TagsListView(GoodsListView):
    def get_queryset(self):
        slug = self.kwargs['slug']
        return super(TagsListView, self).get_queryset().filter(tags__slug=slug)

    def get_context_data(self, **kwargs):
        query_id = self.get_queryset().values_list('id').distinct()
        query = Goods.objects.filter(id__in=query_id)
        context = super(TagsListView, self).get_context_data(**kwargs)
        context['current_tag'] = Tag.objects.filter(slug=self.kwargs['slug'])
        context['categories'] = self.get_queryset().values_list('category__name', 'category__slug').order_by(
            'category__name').distinct()
        manufacturers_id = query.values_list('manufacturer', flat=True).order_by(
            'manufacturer__name').distinct()
        manufacturer_query = Manufacturer.objects.filter(id__in=manufacturers_id)
        manufacturers = []
        for manufacturer in manufacturer_query:
            manufacturers.append((manufacturer, query.filter(manufacturer=manufacturer).count()))
        context['manufacturers'] = manufacturers
        return context


class CategoryListView(GoodsListView):
    def get_queryset(self):
        slug = self.kwargs['slug']
        return super(CategoryListView, self).get_queryset().filter(category__slug=slug)

    def get_context_data(self, **kwargs):
        query_id = self.get_queryset().values_list('id').distinct()
        query = Goods.objects.filter(id__in=query_id)
        context = super(CategoryListView, self).get_context_data(**kwargs)
        context['current_category'] = Category.objects.filter(slug=self.kwargs['slug'])
        manufacturers_id = query.values_list('manufacturer', flat=True).distinct()
        manufacturer_query = Manufacturer.objects.filter(id__in=manufacturers_id)
        manufacturers = []
        for manufacturer in manufacturer_query:
            manufacturers.append((manufacturer, query.filter(manufacturer=manufacturer).count()))
        context['manufacturers'] = manufacturers
        tags_id = query.values_list('tags', flat=True)
        tags_query = Tag.objects.filter(id__in=tags_id)
        tags = []
        for tag in tags_query:
            tags.append((tag, query.filter(tags=tag).count()))
        context['tags'] = tags
        return context


class ManufacturerListView(GoodsListView):
    def get_queryset(self):
        slug = self.kwargs['slug']
        return super(ManufacturerListView, self).get_queryset().filter(manufacturer__slug=slug)

    def get_context_data(self, **kwargs):
        query_id = self.get_queryset().values_list('id').distinct()
        query = Goods.objects.filter(id__in=query_id)
        context = super(ManufacturerListView, self).get_context_data(**kwargs)
        context['current_brand'] = Manufacturer.objects.filter(slug=self.kwargs['slug'])
        context['categories'] = query.values_list('category__name', 'category__slug').order_by(
            'category__name').distinct()
        tags_id = query.values_list('tags', flat=True)
        tags_query = Tag.objects.filter(id__in=tags_id)
        tags = []
        for tag in tags_query:
            tags.append((tag, query.filter(tags=tag).count()))
        context['tags'] = tags
        return context


class ReviewDeleteView(LoginRequiredMixin, SuperuserRequiredMixin, JSONResponseMixin, AjaxResponseMixin, View):
    def post(self, request, *args, **kwargs):
        review_id = request.POST.get('review_id', None)
        try:
            Review.objects.get(id=review_id).delete()
            data = {'status': 'ok', 'message': ['Review deleted']}
            return self.render_json_response(data)
        except Review.DoesNotExist:
            data = {'status': 'fail', 'message': ['Review not found!']}
            return self.render_json_response(data)
