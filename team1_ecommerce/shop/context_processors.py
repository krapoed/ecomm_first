from .models import Category, Tag, Manufacturer
from orders.models import CartItem


def categories(request):
    return {
        'categories_list': Category.objects.all(),
    }


def tags(request):
    return {
        'tags_list': Tag.objects.all(),
    }


def brands(request):
    return {
        'brands_list': Manufacturer.objects.all(),
    }


def items(request):
    count = 0
    for item in CartItem.objects.filter(cart_id=request.session['cart_id']):
        count += item.count
    return {
        'items': CartItem.objects.filter(cart_id=request.session['cart_id']),
        'cart_goods_id': CartItem.objects.filter(cart_id=request.session['cart_id']).values_list('product',
                                                                                                 flat=True).distinct(),
        'item_count': count
    }


def ordering(request):
    sorting = {
        'new': 'New',
        'old': 'Old',
        'rating': 'Rating',
        'price_asc': 'Cheap',
        'price_desc': 'Expensive',
        'has_discount': 'On sales'
    }
    return {'ordering': sorting[request.session.get('sort_order')]}
