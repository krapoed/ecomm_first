import hashlib
import random

from braces.views import LoginRequiredMixin, AnonymousRequiredMixin, AjaxResponseMixin, JSONResponseMixin
from django.core.mail import send_mail
from django.db import transaction
from django.shortcuts import get_object_or_404, redirect
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.utils import timezone

from django.core.urlresolvers import reverse

from django.views.generic import FormView, View, TemplateView, UpdateView, ListView

from .forms import RegistrationForm, UpdateUserForm, UserChangePasswordForm
from .models import UserProfile
from shop.models import Goods


def send_sms(phone_number):
    pass


class RegisterUserView(AnonymousRequiredMixin, FormView):
    form_class = RegistrationForm
    template_name = 'users/register.html'

    def get_authenticated_redirect_url(self):
        messages.warning(self.request, 'Permission denied! Only for anonymous users!')
        return reverse('index')

    @transaction.non_atomic_requests
    def form_valid(self, form):
        with transaction.atomic():

            user = form.save()  # save user to database if form is valid
            username = form.cleaned_data['username']
            email = form.cleaned_data['email']
            phone_number = form.cleaned_data['phone_number']
            activation_type = form.cleaned_data['activation_type']
            salt = hashlib.sha1(str(random.random())).hexdigest()[:5]
            activation_key = hashlib.sha1(salt + email).hexdigest()
            key_expires = timezone.now() + timezone.timedelta(2)
            password = form.cleaned_data['password1']

        if activation_type == 'email':

            user.key_expires = key_expires
            user.activation_key = activation_key
            user.save()

            # Send email with activation key
            email_subject = 'Confirm your registration'

            email_body = "Hey %s, thanks for signing up! " \
                         "To activate your account, click this link within 48 hours %s" % (
                             username,
                             self.request.build_absolute_uri(reverse('register_confirm', kwargs={
                                 'activation_key': activation_key})))
            send_mail(email_subject, email_body, 'info@auto-comm-pvt',
                      [email], fail_silently=False)

            messages.success(self.request, 'Congratulations, %s! You are successfully registered!'
                                           ' Email with confirmation link was been sent.' % username)

            return redirect('index')

        else:

            send_sms(phone_number)

            login(self.request, authenticate(username=username, password=password))
            messages.success(self.request, 'Congratulations, %s! You are successfully registered!'
                                           ' Phone confirmed, account activated!' % username)

            return redirect('index')


def register_confirm(request, activation_key):
    # check if user is already logged in and if he is redirect him to some other url, e.g. home
    if request.user.is_authenticated():
        messages.success(request, 'Confirmation not required!')

        return redirect('index')

    # check if there is UserProfile which matches the activation key (if not then display 404)
    user = get_object_or_404(UserProfile, activation_key=activation_key)

    # check if the activation key has expired, if it has then render confirm_expired.html
    if user.key_expires < timezone.now():
        messages.error(request, 'Sorry, %s, but your activation key has expired!' % user.first_name)

        return redirect('index')

    # if the key hasn't expired save user and set him as active and render some template to confirm activation
    user.activation_key = ''
    user.is_active = True
    user.save()
    messages.success(request, 'Congratulations, %s! Your registration confirmed, now '
                              'your account is active, pleasant shopping!'
                     % user.first_name)

    return redirect('index')


class UserLogin(View):
    def get(self, request):
        messages.warning(request, 'Permission denied! Please login!')
        return redirect('index')

    def post(self, request):
        user = authenticate(username=request.POST['username'], password=request.POST['password'])

        if user:
            login(request, user)
            messages.success(request, 'Logged in, pleasant shopping!')

            return redirect(request.META['HTTP_REFERER'])

        messages.error(request, 'Wrong password or username!')

        return redirect(request.META['HTTP_REFERER'])


def logout_user(request):
    logout(request)
    messages.success(request, 'Logged out, goodbye!')

    return redirect('index')


class UserProfileView(LoginRequiredMixin, TemplateView):
    template_name = 'users/profile.html'


class UserUpdateView(LoginRequiredMixin, UpdateView):
    form_class = UpdateUserForm
    template_name = 'users/update_user.html'

    def get_object(self, queryset=None):
        return self.request.user

    def form_valid(self, form):
        form.save()
        return redirect('user_profile')


class UserChangePassword(LoginRequiredMixin, FormView):
    form_class = UserChangePasswordForm
    template_name = 'users/change_password.html'

    def get_form_kwargs(self):
        kwargs = super(UserChangePassword, self).get_form_kwargs()
        kwargs['user'] = self.request.user
        return kwargs

    def form_valid(self, form):
        form.save()
        messages.success(self.request, 'Password successfully changed! Please login with new password!')
        return redirect('index')


class UserWishListView(LoginRequiredMixin, ListView):
    template_name = 'users/wish_list.html'
    paginate_by = 15
    ordering = 'in_stock'

    def get_queryset(self):
        return self.request.user.wish_list.wish_list.all()


class UserWishListAddView(LoginRequiredMixin, JSONResponseMixin, AjaxResponseMixin, View):
    def add_to_wish_list(self, request, goods_id):
        try:
            goods = Goods.objects.get(id=goods_id)
        except Goods.DoesNotExist:
            return {'status': 'fail', 'message': ['Goods you want to add does not exist!']}
        user = request.user
        try:
            user.wish_list.wish_list.get(id=goods_id)
            return {'status': 'fail', 'message': ['Goods you want to add, already in your wish list!']}
        except Goods.DoesNotExist:
            user.wish_list.wish_list.add(goods)
            user.save()
            return {'status': 'ok', 'message': [goods.name + ' was successfully added to your wish list!']}

    def post(self, request, *args, **kwargs):
        goods_id = kwargs.get('goods_id', None)
        result = self.add_to_wish_list(request, goods_id)
        if result['status'] == 'ok':
            messages.success(request, result['message'])
        else:
            messages.error(request, result['message'])
        return redirect(request.META['HTTP_REFERER'])

    def post_ajax(self, request, *args, **kwargs):
        goods_id = kwargs.get('goods_id', None)
        return self.render_json_response(self.add_to_wish_list(request, goods_id))


class UserWishListDelView(LoginRequiredMixin, JSONResponseMixin, AjaxResponseMixin, View):
    def del_from_wish_list(self, request, goods_id):
        user = request.user
        try:
            goods = Goods.objects.get(id=goods_id)
            user.wish_list.wish_list.get(id=goods_id)
        except Goods.DoesNotExist:
            return {'status': 'fail', 'message': ['Goods you want to remove, does not exist!']}
        user.wish_list.wish_list.remove(goods)
        user.save()
        return {'status': 'ok', 'message': [goods.name + ' was successfully removed from your wish list!']}

    def post(self, request, *args, **kwargs):
        goods_id = kwargs.get('goods_id', None)
        result = self.del_from_wish_list(request, goods_id)
        if result['status'] == 'ok':
            messages.success(request, result['message'])
        else:
            messages.error(request, result['message'])
        return redirect(request.META['HTTP_REFERER'])

    def post_ajax(self, request, *args, **kwargs):
        goods_id = kwargs.get('goods_id', None)
        return self.render_json_response(self.del_from_wish_list(request, goods_id))
