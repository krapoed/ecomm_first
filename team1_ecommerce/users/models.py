from django.contrib.auth.models import User, AbstractUser
from django.db import models
from django.utils import timezone


class WishList(models.Model):
    from shop.models import Goods
    name = models.CharField(max_length=50)
    wish_list = models.ManyToManyField(Goods, blank=True)

    def __unicode__(self):
        return self.name


class UserProfile(AbstractUser):
    wish_list = models.OneToOneField(WishList)
    activation_key = models.CharField(max_length=40, blank=True)
    key_expires = models.DateTimeField(default=timezone.now)
    phone_number = models.CharField(max_length=20, blank=True)
    rating = models.FloatField(default=0)

    def delete(self, using=None):
        super(UserProfile, self).delete(using=using)
        self.wish_list.delete()

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if not self.pk:
            wish_list = WishList(name=self.username)
            wish_list.save()
            self.wish_list = wish_list
        super(UserProfile, self).save(force_insert=force_insert, force_update=force_update, using=using,
                                      update_fields=update_fields)

    def get_full_name(self):
        """
        Returns the first_name plus the last_name, with a space in between.
        """
        full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        """Returns the short name for the user."""
        if self.first_name:
            return self.first_name
        else:
            return self.username

    def __unicode__(self):
        return self.username

    class Meta:
        verbose_name = 'User profile'
        verbose_name_plural = 'User profiles'
