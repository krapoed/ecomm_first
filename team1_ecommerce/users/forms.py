from django import forms
from django.contrib.auth.forms import UserCreationForm, PasswordChangeForm

from users.models import UserProfile
from users.validators import validate_password, validate_phone_number


class UpdateUserForm(forms.ModelForm):
    class Meta:
        model = UserProfile
        fields = ('first_name', 'last_name', 'email', 'phone_number')

    email = forms.EmailField(required=True)
    phone_number = forms.CharField(required=False, validators=[validate_phone_number])

    def clean_email(self):
        email = self.cleaned_data["email"]
        if email == self.instance.email:
            return email
        try:
            UserProfile.objects.get(email=email)
        except UserProfile.DoesNotExist:
            return email
        self.add_error('email', 'Email address already use')

        # def clean_phone_number(self):
        #     phone_number = self.cleaned_data['phone_number']
        #     if phone_number:
        #         if not phone_number.startswith('+'):
        #             self.add_error('phone_number', 'Phone number must start with \"+\".')
        #         if len(phone_number) < 7:
        #             self.add_error('phone_number', 'Phone number too short.')
        #         if not phone_number.strip('-+() ').isdigit():
        #             self.add_error('phone_number',
        #                            'Phone number may contain only \"+\", \"-\", \"(\", \")\", space and digits.')
        #     return phone_number


class UserChangePasswordForm(PasswordChangeForm):
    new_password1 = forms.CharField(validators=[validate_password])


class RegistrationForm(UserCreationForm):
    class Meta:
        model = UserProfile
        fields = ('first_name', 'last_name', 'email', 'username', 'phone_number', 'password1', 'password2')

    ACTIVATION_CHOICE = (
        ('email', 'Email confirmation'),
        ('phone', 'Phone confirmation'),
    )

    email = forms.EmailField(required=True)
    first_name = forms.CharField(required=True)
    last_name = forms.CharField(required=True)
    phone_number = forms.CharField(required=False, validators=[validate_phone_number])
    activation_type = forms.TypedChoiceField(choices=ACTIVATION_CHOICE, widget=forms.RadioSelect, initial='email')
    password1 = forms.CharField(validators=[validate_password])

    # clean email field
    def clean_email(self):

        email = self.cleaned_data["email"]
        try:

            UserProfile.objects.get(email=email)

        except UserProfile.DoesNotExist:

            return email

        raise forms.ValidationError('Email address already use')

    def clean(self):

        cleaned_data = super(RegistrationForm, self).clean()
        phone_number = cleaned_data.get('phone_number', None)

        if self.cleaned_data['activation_type'] == 'phone':
            if not phone_number:
                self.add_error('phone_number', 'This field is required.')

    # modify save() method so that we can set user.is_active to False when we first create our user
    def save(self, commit=True):

        user = super(RegistrationForm, self).save(commit=False)
        user.email = self.cleaned_data['email']

        if commit:
            if self.cleaned_data['activation_type'] == 'email':
                user.is_active = False  # not active until he opens activation link

            user.save()

        return user
