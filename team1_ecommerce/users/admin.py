from django.contrib import admin

from .models import UserProfile, WishList

# Register your models here.

admin.site.register(UserProfile)
admin.site.register(WishList)
