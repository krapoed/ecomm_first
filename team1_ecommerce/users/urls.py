from django.conf.urls import url
from django.contrib import admin
from django.views.generic import RedirectView

from .views import register_confirm, logout_user, RegisterUserView, UserLogin, UserProfileView, UserUpdateView, \
    UserChangePassword, UserWishListView, UserWishListAddView, UserWishListDelView

admin.autodiscover()

urlpatterns = [
    url(r'^$', RedirectView.as_view(pattern_name='user_profile', permanent=True), name='user_index'),
    url(r'^sign_up/$', RegisterUserView.as_view(), name='register_user'),
    url(r'^sign_in/$', UserLogin.as_view(), name='login_user'),
    url(r'^profile/$', UserProfileView.as_view(), name='user_profile'),
    url(r'^password/$', UserChangePassword.as_view(), name='user_change_password'),
    url(r'^edit/$', UserUpdateView.as_view(), name='user_update'),
    url(r'^wish_list/$', UserWishListView.as_view(), name='user_wish_list'),
    url(r'^wish_list/add/(?P<goods_id>\d+)/$', UserWishListAddView.as_view(), name='user_wish_list_add'),
    url(r'^wish_list/del/(?P<goods_id>\d+)/$', UserWishListDelView.as_view(), name='user_wish_list_del'),
    url(r'^sign_out/$', logout_user, name='logout_user'),
    url(r'^confirm/(?P<activation_key>\w+)/$', register_confirm, name='register_confirm'),
]
