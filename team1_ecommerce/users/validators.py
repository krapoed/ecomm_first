from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _

SYMBOLS = ['!', '#', '$', '%', '^', '&', '*', '(', ')', '-', '_', '=', '+']
NUMBERS = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']


def validate_password(value):
    """
    Validator for password field
    :param value:
    :raise ValidationError:
    """
    num_err = True
    cap_err = True
    lower_err = True
    symb_err = True
    errors = []

    if not value:
        raise ValidationError(_("This field is required."))

    for p in value:
        if p in SYMBOLS:
            symb_err = False

        if p.islower():
            lower_err = False

        if p in NUMBERS:
            num_err = False

        if p.isupper():
            cap_err = False

    if symb_err:
        errors.append(ValidationError(_("Password doesn't contain any symbol (%s)." % str(SYMBOLS))))

    if lower_err:
        errors.append(ValidationError(_("Password doesn't contain any lowercase character.")))

    if cap_err:
        errors.append(ValidationError(_("Password doesn't contain any capital character.")))

    if num_err:
        errors.append(ValidationError(_("Password doesn't contain any number.")))

    if len(value) < 6:
        errors.append(ValidationError(_("Password too short, need 6 or more.")))

    if errors:
        raise ValidationError(errors)


def validate_phone_number(value):
    """
    Validator for phone_number field
    :param value:
    """
    errors = []

    if not value:
        raise ValidationError(_("This field is required."))

    if not value.startswith('+'):
        errors.append(ValidationError(_('Phone number must start with \"+\".')))
    if len(value) < 7:
        errors.append(ValidationError(_('Phone number too short.')))
    if not value.strip('-+() ').isdigit():
        errors.append(ValidationError(_('Phone number may contain only \"+\", \"-\", \"(\", \")\", space and digits.')))

    if errors:
        raise ValidationError(errors)
