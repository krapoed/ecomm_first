from django.core.management.base import BaseCommand
from django.utils import timezone

from users.models import UserProfile


class Command(BaseCommand):
    help = 'Delete inactive users accounts'

    def handle(self, *args, **options):
        for user in UserProfile.objects.all():
            if not user.is_active:
                if user.key_expires < timezone.now():
                    self.stdout.write('Removing user "%s"' % user.username)
                    user.delete()
