import random

LIST_VIEW = [
    'normal',
    'string'
]


class CollectUserInfo(object):
    def process_request(self, request):
        if 'cart_id' not in request.session:
            request.session['cart_id'] = self._create_cart_id()
        if 'user_id' not in request.session:
            request.session['user_id'] = self._create_user_id()

        if not request.session.get('sort_order'):
            request.session['sort_order'] = 'new'
        if not request.session.get('list_view'):
            request.session['list_view'] = 'normal'
        if not request.session.get('per_page'):
            if request.session['list_view'] == 'normal':
                request.session['per_page'] = '6'
            else:
                request.session['per_page'] = '12'

    def _create_cart_id(self):
        cart_id = ''
        characters = 'ABCDEFGHIJKLMNOPQRQSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890!@#$%^&*()'
        cart_id_length = 50
        for y in range(cart_id_length):
            cart_id += characters[random.randint(0, len(characters) - 1)]
        return cart_id

    def _create_user_id(self):
        user_id = ''
        characters = 'ABCDEFGHIJKLMNOPQRQSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890!@#$%^&*()'
        user_id_length = 50
        for y in range(user_id_length):
            user_id += characters[random.randint(0, len(characters) - 1)]
        return user_id
