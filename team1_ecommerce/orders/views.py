from braces.views import LoginRequiredMixin, AjaxResponseMixin, JSONResponseMixin
from django.core.urlresolvers import reverse_lazy
from django.shortcuts import redirect
from django.db import transaction
from django.views.generic.list import ListView
from django.views.generic.edit import FormView, View, DeleteView
from django.contrib import messages

from .models import CartItem, Order
from shop.models import Goods
from .forms import OrderForm


class AddProductToItem(JSONResponseMixin, AjaxResponseMixin, View):
    def post(self, request, *args, **kwargs):
        count = int(request.POST['count'])
        try:
            CartItem.objects.get(cart_id=request.session['cart_id'], product=Goods.objects.get(id=request.POST['pk']))
        except CartItem.DoesNotExist:
            if request.user.is_authenticated():
                price = Goods.objects.get(id=request.POST['pk']).price - Goods.objects.get(
                    id=request.POST['pk']).price * self.request.user.rating / 100
            else:
                price = Goods.objects.get(id=request.POST['pk']).price
            CartItem.objects.create(cart_id=request.session['cart_id'],
                                    product=Goods.objects.get(id=request.POST['pk']), price=price, count=count)
            messages.info(request, 'Product added to cart.')
            return redirect(request.META['HTTP_REFERER'])
        messages.info(request, 'Product already added to cart.')
        return redirect(request.META['HTTP_REFERER'])

    def post_ajax(self, request, *args, **kwargs):
        item_id = request.POST.get('pk', None)
        if item_id:
            try:
                CartItem.objects.get(cart_id=request.session['cart_id'],
                                     product=Goods.objects.get(id=request.POST['pk']))
                data = {'status': 'fail', 'message': ['Goods already in you cart!']}
                return self.render_json_response(data)
            except CartItem.DoesNotExist:
                if request.user.is_authenticated():
                    price = Goods.objects.get(id=request.POST['pk']).price - Goods.objects.get(
                        id=request.POST['pk']).price * self.request.user.rating / 100
                else:
                    price = Goods.objects.get(id=request.POST['pk']).price
                CartItem.objects.create(cart_id=request.session['cart_id'],
                                        product=Goods.objects.get(id=request.POST['pk']), price=price)
                data = {'status': 'ok', 'message': ['Goods successfully added to your cart!']}
                return self.render_json_response(data)
        data = {'status': 'fail', 'message': ['Goods no exist!']}
        return self.render_json_response(data)


class Cart(FormView):
    template_name = 'orders/cart.html'
    form_class = OrderForm
    success_url = reverse_lazy('index')

    def get_context_data(self, **kwargs):
        context = super(Cart, self).get_context_data(**kwargs)
        context['items'] = CartItem.objects.filter(cart_id=self.request.session['cart_id']).order_by('product')
        context['total'] = 0
        for item in CartItem.objects.filter(cart_id=self.request.session['cart_id']):
            context['total'] += item.get_cost()
        try:
            order = Order.objects.get(cart_id=self.request.session['cart_id'])
            context['editable'] = order.editable
            return context
        except Order.DoesNotExist:
            return context

    def get_initial(self):
        initial = super(Cart, self).get_initial()
        try:
            order = Order.objects.get(cart_id=self.request.session['cart_id'])
            if order.editable:
                initial['place'] = order.place
                initial['date'] = order.date
                initial['time'] = order.time
                initial['phone_number'] = order.phone_number
                initial['description'] = order.description
        except Order.DoesNotExist:
            if self.request.user.is_authenticated():
                initial['phone_number'] = self.request.user.phone_number
        return initial

    @transaction.non_atomic_requests
    def form_valid(self, form):
        with transaction.atomic():
            usr_id = self.request.session['user_id']
            cart_id = self.request.session['cart_id']
            place = form.cleaned_data['place']
            date = form.cleaned_data['date']
            time = form.cleaned_data['time']
            description = form.cleaned_data['description']
            phone_number = form.cleaned_data['phone_number']
            try:
                order = Order.objects.get(cart_id=cart_id)
                order.update(place=place, date=date, time=time, description=description,
                             phone_number=phone_number, status='Waiting', editable=False)
                messages.info(self.request, 'Order update.')
            except Order.DoesNotExist:
                if self.request.user.is_authenticated():
                    user = self.request.user
                    Order.objects.create(user=user, usr_id=usr_id, cart_id=cart_id,
                                         place=place, date=date, time=time, description=description,
                                         phone_number=phone_number, editable=False)
                else:
                    Order.objects.create(usr_id=usr_id, cart_id=cart_id,
                                         place=place, date=date, time=time, description=description,
                                         phone_number=phone_number, editable=False)
                messages.info(self.request, 'Order create.')
            del self.request.session['cart_id']
            return super(Cart, self).form_valid(form)


class AddOne(JSONResponseMixin, AjaxResponseMixin, View):
    def post(self, request, *args, **kwargs):
        try:
            item = CartItem.objects.get(cart_id=request.session['cart_id'], id=request.POST['pk'])
            item.add_one()
            messages.success(self.request, 'Increased count of product.')
            return redirect(request.META['HTTP_REFERER'])
        except CartItem.DoesNotExist:
            messages.error(self.request, 'Can\'t increase count of product.')
            return redirect(request.META['HTTP_REFERER'])

    def post_ajax(self, request, *args, **kwargs):
        item_id = request.POST.get('pk', None)
        if item_id:
            try:
                item = CartItem.objects.get(cart_id=request.session['cart_id'], id=item_id)
                item.add_one()
                data = {'status': 'ok', 'message': ['Increased count of product.']}
                return self.render_json_response(data)
            except CartItem.DoesNotExist:
                pass
        data = {'status': 'fail', 'message': ['Goods no exist!']}
        return self.render_json_response(data)


class DeleteOne(JSONResponseMixin, AjaxResponseMixin, View):
    def post(self, request, *args, **kwargs):
        try:
            item = CartItem.objects.get(cart_id=request.session['cart_id'], id=request.POST['pk'])
            if item.count > 1:
                item.delete_one()
                messages.success(self.request, 'Reduced count of product.')
                return redirect(request.META['HTTP_REFERER'])
            messages.error(self.request, 'Can\'t reduce count of product.')
            return redirect(request.META['HTTP_REFERER'])
        except CartItem.DoesNotExist:
            messages.error(self.request, 'Can\'t reduce count of product.')
            return redirect(request.META['HTTP_REFERER'])

    def post_ajax(self, request, *args, **kwargs):
        item_id = request.POST.get('pk', None)
        if item_id:
            try:
                item = CartItem.objects.get(cart_id=request.session['cart_id'], id=item_id)
                if item.count > 1:
                    item.delete_one()
                    data = {'status': 'ok', 'message': ['Reduced count of product.']}
                    return self.render_json_response(data)
                else:
                    data = {'status': 'fail', 'message': ['Too low count of product to reduce. (' + str(item.count) +
                                                          ') Try to delete it.']}
                    return self.render_json_response(data)
            except CartItem.DoesNotExist:
                pass
        data = {'status': 'fail', 'message': ['Goods no exist!']}
        return self.render_json_response(data)


class EditOrder(LoginRequiredMixin, View):
    def post(self, request, *args, **kwargs):
        try:
            order = Order.objects.get(id=request.POST['pk'])
            if order.set_editable():
                if request.session['cart_id'] == order.cart_id:
                    return redirect('index_cart')
                else:
                    request.session['cart_id'] = order.cart_id
                    messages.success(request, 'Now you can change your order.')
                    return redirect('index_cart')
            else:
                messages.error(request, 'Can\'t change order. Save previosly editable order.')
                return redirect(request.META['HTTP_REFERER'])
        except Order.DoesNotExist:
            messages.error(request, 'Something gone wrong.')
            return redirect(request.META['HTTP_REFERER'])


class DeactivateOrder(LoginRequiredMixin, View):
    def post(self, request, *args, **kwargs):
        try:
            order = Order.objects.get(id=self.kwargs['id'])
            order.set_unactive()
            order.save()
            return redirect(request.META['HTTP_REFERER'])
        except CartItem.DoesNotExist:
            return redirect(request.META['HTTP_REFERER'])


class ActivateOrder(LoginRequiredMixin, View):
    def post(self, request, *args, **kwargs):
        try:
            order = Order.objects.get(id=self.kwargs['id'])
            order.set_active()
            order.save()
            return redirect(request.META['HTTP_REFERER'])
        except CartItem.DoesNotExist:
            return redirect(request.META['HTTP_REFERER'])


class DeleteOrder(LoginRequiredMixin, DeleteView):
    model = Order
    success_url = reverse_lazy('index_orders')
    success_message = 'Order been deleted.'

    def delete(self, request, *args, **kwargs):
        messages.success(self.request, self.success_message)
        return super(DeleteOrder, self).delete(request, *args, **kwargs)


class DeleteItem(DeleteView):
    model = CartItem
    success_url = reverse_lazy('index_cart')
    success_message = 'Item been deleted.'

    def delete(self, request, *args, **kwargs):
        messages.success(self.request, self.success_message)
        return super(DeleteItem, self).delete(request, *args, **kwargs)


class ShowOrderView(ListView):
    template_name = 'orders/show_order.html'

    def get_context_data(self, **kwargs):
        context = super(ShowOrderView, self).get_context_data(**kwargs)
        context['info'] = Order.objects.get(id=self.kwargs['id'])
        context['total'] = 0
        for item in CartItem.objects.filter(order=context['info']):
            context['total'] += item.get_cost()
        return context

    def get_queryset(self, *args, **kwargs):
        cart_id = Order.objects.get(id=self.kwargs['id']).cart_id
        order = CartItem.objects.filter(cart_id=cart_id).order_by('product')
        return order


class OrdersView(LoginRequiredMixin, ListView):
    model = Order

    def get_queryset(self):
        order = Order.objects.filter(user=self.request.user)
        return order
