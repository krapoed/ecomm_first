from django import template

register = template.Library()

def price(value):
    return value.get_price()

register.filter('price', price)

