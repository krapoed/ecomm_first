from .models import Order
from users.validators import validate_phone_number
from django import forms


class OrderForm(forms.ModelForm):
    class Meta:
        model = Order
        fields = ('place', 'date', 'time', 'description', 'phone_number')
        widgets = {
            'place': forms.TextInput(attrs={'class': 'form-control'}),
            'date': forms.DateInput(attrs={'class': 'form-control', 'id': 'datepicker'}),
            'time': forms.Select(attrs={'class': 'form-control'}),
            'description': forms.Textarea(attrs={'class': 'form-control'}),
        }

    phone_number = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}), required=True,
                                   validators=[validate_phone_number], min_length=13, max_length=13)

