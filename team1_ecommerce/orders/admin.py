from django.contrib import admin

from .models import Order, CartItem

class CartItemInLine(admin.TabularInline):
    model = CartItem
    extra = 0
    fields = ['product', 'count', 'price']
    readonly_fields = ['product', 'count', 'price']
    can_delete = False

    def has_add_permission(self, request):
        return False

class OrderAdmin(admin.ModelAdmin):
    inlines = [CartItemInLine]
    fieldsets = ((None, {'fields': ('user', 'usr_id', 'place', 'date', 'time', 'description',
                                    'phone_number', 'status')}),
                 ('Service info', {
                     'classes': ('collapse',),
                     'fields': ('cart_id', 'editable')
                 }),
                 )
    search_fields = ['id']
    list_display = ['id', 'user', 'phone_number', 'date', 'time', 'status']
    list_filter = ('user', 'status')
    readonly_fields = ['user', 'editable']



admin.site.register(Order, OrderAdmin)
admin.site.register(CartItem)
