from django.conf.urls import url

import views

urlpatterns = [
    url(r'^add_to_cart/$', views.AddProductToItem.as_view(), name='to_cart'),
    url(r'^add_one/$', views.AddOne.as_view(), name='add_one'),
    url(r'^delete_one/$', views.DeleteOne.as_view(), name='delete_one'),
    url(r'^cart/$', views.Cart.as_view(), name='index_cart'),
    url(r'^$', views.OrdersView.as_view(), name='index_orders'),
    url(r'^order_edit/$', views.EditOrder.as_view(), name='edit_order'),
    url(r'^order_delete/(?P<pk>[-\w]+)/$', views.DeleteOrder.as_view(), name='delete_order'),
    url(r'^item_delete/(?P<pk>[-\w]+)/$', views.DeleteItem.as_view(), name='delete_item'),
    url(r'^order_show/(?P<id>[-\w]+)/$', views.ShowOrderView.as_view(), name='show_order'),
    url(r'^order_deactivate/(?P<id>[-\w]+)/$', views.DeactivateOrder.as_view(), name='deactivate_order'),
    url(r'^order_activate/(?P<id>[-\w]+)/$', views.ActivateOrder.as_view(), name='activate_order'),

]
