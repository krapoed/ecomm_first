from django.db import models
from django.db.models.signals import pre_save, post_save
from django.dispatch import receiver

from shop.models import Goods
from users.models import UserProfile


class Order(models.Model):


    CHOICES_STATUS = (
        ('Unactive', 'Unactive'),
        ('Waiting', 'Waiting'),
        ('Editing', 'Editing'),
        ('In progress', 'In progress'),
        ('Approve', 'Approve'),
        ('Complete', 'Complete'),
        ('Canceled', 'Canceled'),
    )

    TIME_CHOICE = (
        ('10:00 - 14:00', '10:00 - 14:00'),
        ('14:00 - 18:00', '14:00 - 18:00'),
        ('16:00 - 21:00', '16:00 - 21:00'),
        ('18:00 - 21:00', '18:00 - 21:00'),
    )


    user = models.ForeignKey(UserProfile, blank=True, null=True)
    usr_id = models.CharField(max_length=50, blank=True)
    cart_id = models.CharField(max_length=50, blank=False)
    place = models.TextField(max_length=200, blank=False)
    date = models.DateField()
    time = models.CharField(max_length=13, choices=TIME_CHOICE, blank=False)
    description = models.TextField(max_length=200, blank=True)
    phone_number = models.CharField(max_length=13, blank=False)
    status = models.CharField(max_length=11, choices=CHOICES_STATUS, default='Waiting')
    editable = models.BooleanField(default=False)

    def get_price(self):
        price = 0
        for product in CartItem.objects.filter(cart_id=self.cart_id):
            price += product.get_cost()
        return "{:10.2f}".format(price)

    def update(self, place, date, time, description, phone_number, status, editable):
        self.place = place
        self.date = date
        self.time = time
        self.description = description
        self.phone_number = phone_number
        self.status = status
        self.editable = editable
        self.save()

    def set_unactive(self):
        self.status = 'Unactive'
        self.save()

    def set_active(self):
        self.status = 'Waiting'
        self.save()

    def set_editable(self):
        if Order.objects.filter(status='Editing').exclude(id=self.id):
            return False
        else:
            self.editable = True
            self.status = 'Editing'
            self.save()
            return True

    def __unicode__(self):
        return str(self.id)


class CartItem(models.Model):
    cart_id = models.CharField(max_length=50)
    product = models.ForeignKey(Goods)
    order = models.ForeignKey(Order, blank=True, null=True)
    price = models.FloatField(default=0)
    count = models.IntegerField(default=1)

    def add_one(self):
        self.count += 1
        self.save()

    def delete_one(self):
        self.count -= 1
        self.save()

    def get_cost(self):
        return float(self.count * self.price)

    def __unicode__(self):
        return self.product.name + '_' + str(self.order)


@receiver(pre_save, sender=Order)
def check_status(sender, instance, **kwargs):
    if instance.id and instance.user:
        if instance.status == 'Complete' and Order.objects.get(pk=instance.id).status != 'Complete':
            instance.user.rating += 1
            instance.user.save()
        elif instance.status != 'Complete' and Order.objects.get(pk=instance.id).status == 'Complete':
            instance.user.rating -= 1
            instance.user.save()

@receiver(post_save, sender=Order)
def set_order_in_cartitem(sender, instance, **kwargs):
    for item in CartItem.objects.filter(cart_id=instance.cart_id).filter(order=None):
        item.order = instance
        item.save()