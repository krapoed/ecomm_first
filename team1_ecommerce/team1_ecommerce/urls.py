"""team1_ecommerce URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.contrib import admin
from django.conf.urls.static import static

from shop import urls as shop_urls
from shop import views
from users import urls as users_urls
from orders import urls as orders_urls
from team1_ecommerce import settings

urlpatterns = [
                  url(r'^$', views.shop_index, name='index'),
                  url(r'^grappelli/', include('grappelli.urls')),
                  url(r'^admin/', include(admin.site.urls)),
                  url(r'^shop/', include(shop_urls)),
                  url(r'^users/', include(users_urls)),
                  url(r'^orders/', include(orders_urls)),

              ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
