from fabric.api import env, local, run, cd, sudo


def vagrant():
    env.user = 'vagrant'
    env.hosts = ['127.0.0.1:2222']
    result = local('vagrant ssh-config | grep IdentityFile', capture=True)
    env.key_filename = result.split()[1]


def uname():
    run('uname -a')


def deploy():
    with cd('/opt/'):
        sudo("git clone https://bitbucket.org/Brialius/pvt-python-ecomm-auto.git")
        sudo("chown -R vagrant:vagrant pvt-python-ecomm-auto/")
        sudo("usermod -a -G www-data vagrant")

    with cd('/opt/pvt-python-ecomm-auto/'):
        sudo("pip install -r requirements.txt")
        sudo('cp -rf nginx_config /etc/nginx/sites-available/default')

    # static, media ---> /var/www/nginx/
    with cd('/opt/pvt-python-ecomm-auto/team1_ecommerce'):
        sudo('mkdir -p /var/www/nginx/media/')
        sudo('./manage.py collectstatic')
        sudo('chown -R www-data:www-data /var/www/nginx/')
        sudo('chmod g+w /var/www/nginx/media/')
        run('./manage.py makemigrations')
        run('./manage.py migrate')
        run('./manage.py createsuperuser')
        # nginx log ---> /var/log/nginx/autosite
        sudo('chown www-data:www-data /var/log/nginx/')
        sudo('chmod 775 /var/log/nginx/')
        print("[INFO]Deployment has been accomplished.")
        print("******Use")
        print("           ---> fab vagrant uname start <---")
        print("                                                to start web server******")


def start():
    with cd('/opt/pvt-python-ecomm-auto/team1_ecommerce'):
        run('echo -e "\nnginx test"')
        sudo('nginx -t')
        sudo('service nginx restart')
        run('ifconfig | grep Mask:')
        run('gunicorn team1_ecommerce.wsgi:application')
