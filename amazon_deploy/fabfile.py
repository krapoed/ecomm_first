import re

from fabric.api import env, local, run, cd, sudo, put


def vagrant():
    result = local('vagrant ssh-config', capture=True)
    hostname = re.findall(r'HostName\s+([^\r\n]+)', result)[0]
    port = re.findall(r'Port\s+([^\r\n]+)', result)[0]
    env.hosts = ['%s:%s' % (hostname, port)]
    env.user = re.findall(r'User\s+([^\r\n]+)', result)[0]
    env.key_filename = re.findall(r'IdentityFile\s+([^\r\n]+)', result)[0]


def uname():
    run('uname -a')


def deploy():
    with cd('/opt/'):
        sudo("git clone https://bitbucket.org/Brialius/pvt-python-ecomm-auto.git")
        sudo("chown -R ubuntu:ubuntu pvt-python-ecomm-auto/")
        sudo("usermod -a -G www-data ubuntu")

    with cd('/opt/pvt-python-ecomm-auto/'):
        sudo("pip install -r requirements.txt")
        sudo('cp -f ./amazon_deploy/nginx_config /etc/nginx/sites-available/default')
        sudo('cp -f ./amazon_deploy/team1_ecommerce_wsgi /etc/gunicorn.d/team1_ecommerce')

    # static, media ---> /var/www/nginx/
    with cd('/opt/pvt-python-ecomm-auto/team1_ecommerce'):
        sudo('mkdir -p /var/www/nginx/media/')
        sudo('mkdir -p /var/www/nginx/static/')
        sudo('chown -R www-data:www-data /var/www/nginx/')
        sudo('chmod g+w /var/www/nginx/media/')
        sudo('./manage.py collectstatic')
        run('./manage.py makemigrations')
        run('./manage.py migrate')
        run('./manage.py createsuperuser')
        sudo('chown www-data:www-data /var/log/nginx/')
        sudo('chmod 775 /var/log/nginx/')
        put('crontab', '/tmp/crontab')
        run('crontab < /tmp/crontab')
        print("[INFO]Deployment has been accomplished.")
        print("******Use fab vagrant start to start web server******")


def git_pull():
    with cd('/opt/pvt-python-ecomm-auto/'):
        run("git pull")
    with cd('/opt/pvt-python-ecomm-auto/team1_ecommerce'):
        sudo('./manage.py collectstatic')
        run('./manage.py makemigrations')
        run('./manage.py migrate')
    start()


def start():
    sudo('service nginx restart', pty=False)
    sudo('service gunicorn restart', pty=False)
    print 'Site address: http://' + env.host.split(':')[0] + '/'
